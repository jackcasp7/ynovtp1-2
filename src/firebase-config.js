// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCbQ8FJaqHPYdtPvyNA_xTzLe6brCvolUM",
  authDomain: "ynov-tp1-2.firebaseapp.com",
  projectId: "ynov-tp1-2",
  storageBucket: "ynov-tp1-2.appspot.com",
  messagingSenderId: "344008331804",
  appId: "1:344008331804:web:a2b841b2e7f5367f5d05d9",
  measurementId: "G-M3W0NY6EF2"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
